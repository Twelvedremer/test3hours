//
//  Producto.swift
//  Test3hours
//
//  Created by Momentum Lab 1 on 2/14/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation


class Producto{
    var nombre:String!
    var precio:Float!
    var stock:Int!
    var enCaja:Int = 0
    
    init(_ name:String, _ price:Float, _ stock: Int) {
        self.nombre = name
        self.precio = price
        self.stock = stock
    }
    
    func comprarItem() -> Int {
        self.enCaja += 1
        self.stock = self.stock-1
        return self.stock
    }
    
    func DevolverItems(){
        self.stock = self.stock + self.enCaja
        self.enCaja = 0
    }
    
    func HayEnCaja() -> Bool{
        return enCaja > 0 ? true: false
    }
    
    func HayEnStock() -> Bool {
        return stock > 0 ? true: false
    }
    
    func costoTotal() -> Float {
        return Float(enCaja) * precio
    }
    
    
}
