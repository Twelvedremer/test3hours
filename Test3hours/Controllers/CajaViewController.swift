//
//  CajaViewController.swift
//  Test3hours
//
//  Created by Momentum Lab 1 on 2/14/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol CajaViewDelegate {
    func actualizarBd(row:Int)
}

class CajaViewController: UIViewController {

    
    @IBOutlet weak var table: UITableView!
    var onSaleShop: [(Producto,Int)] = []
    var delegate : CajaViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    extension CajaViewController: UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return onSaleShop.count
          
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cajaCell") as! CajaTableViewCell
    
                cell.name.text = self.onSaleShop[indexPath.row].0.nombre
                cell.cantidad.text = "\(self.onSaleShop[indexPath.row].0.enCaja)"
                cell.indice = indexPath.row
                cell.delegate = self
            
            
            
            return cell
        }
        
    }

    
extension CajaViewController: CajaCellDelegate{
    
    func RefrescarTabla(){
        table.reloadData()
    }
    func RegresarItem(indice:Int){
        
        let alert = UIAlertController(title: "Alerta", message: "Deseas regresar el item?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
        self.delegate.actualizarBd(row: self.onSaleShop[indice].1)
        self.onSaleShop.remove(at: indice)
        self.table.reloadData()
        }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
    present(alert, animated: true, completion: nil)
    }
    
}

