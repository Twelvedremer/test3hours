//
//  PrincipalTableViewCell.swift
//  Test3hours
//
//  Created by Momentum Lab 1 on 2/14/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol PrincipalCellDelegate {
    func RefrescarTabla()
    func ComprarItem(indice:Int)-> Int
}

class PrincipalTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var stock: UILabel!
    var indice:Int = 0
    var delegate:PrincipalCellDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func comprar(){
       let stockValue = delegate.ComprarItem(indice: indice)
        stock.text = "\(stockValue-1)"
        delegate.RefrescarTabla()
       
    }

}
