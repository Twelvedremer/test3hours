//
//  CajaTableViewCell.swift
//  Test3hours
//
//  Created by Momentum Lab 1 on 2/14/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit


protocol CajaCellDelegate {
    func RefrescarTabla()
    func RegresarItem(indice:Int)
}

class CajaTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    
    var indice:Int = 0
    var delegate:CajaCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func borrarItem(){
        self.delegate.RegresarItem(indice: self.indice)
    }

}
