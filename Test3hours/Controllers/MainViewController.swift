//
//  MainViewController.swift
//  Test3hours
//
//  Created by Momentum Lab 1 on 2/14/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var itemsLabel: UIButton!
    @IBOutlet weak var costLabel: UILabel!
    
    var stockShop: [Producto] = []
    var stockDisponible: [(Producto,Int)] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        cargarDatos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "idSegue" {
            let nextScene =  segue.destination as! CajaViewController
            nextScene.delegate = self
            var datas: [(Producto,Int)] = []
            for i in 0 ..< stockShop.count{
                if stockShop[i].HayEnCaja(){
                    datas.append((stockShop[i],i))
                }
            }
            nextScene.onSaleShop = datas
        }
    }
    
}
    extension MainViewController: UITableViewDataSource{
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return stockDisponible.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell") as! PrincipalTableViewCell
            let row = indexPath.row
            if let name = self.stockDisponible[row].0.nombre , let cost = self.stockDisponible[row].0.precio,  let stock = self.stockDisponible[row].0.stock {
                cell.cost.text = "\(cost) Bs.f"
                cell.name.text = name
                cell.stock.text = "\(stock)"
                cell.indice = row
                cell.delegate = self
            }
    
            
            return cell
        }
        
    }

extension MainViewController{

    func cargarDatos(){
        if let path = Bundle.main.path(forResource: "Productos", ofType: "plist") {
            ////If your plist contain root as Dictionary
            if let dic = NSDictionary(contentsOfFile: path){
                var i = 0
                for data in dic.object(forKey: "Productos") as! [NSDictionary]{
                    let temporal = Producto(data.object(forKey: "name") as! String, data.object(forKey: "cost") as! Float, data.object(forKey: "stock") as! Int)
                    if temporal.HayEnStock(){
                        stockShop.append(temporal)
                        stockDisponible.append((temporal,i))
                        i = i+1
                    }
                }
            }
        }
}
}

extension MainViewController: PrincipalCellDelegate{

    func RefrescarTabla(){
        table.reloadData()
    }
    func ComprarItem(indice:Int)-> Int {
        itemsLabel.setTitle("\(Int(itemsLabel.currentTitle!)!+1)",for: .normal)
        costLabel.text = "\(Float(costLabel.text!)!+stockDisponible[indice].0.precio)"
        let resultado = stockDisponible[indice].0.comprarItem()
        if resultado > 0 {
            return stockShop[(stockDisponible[indice].1)].stock
        } else {
            stockDisponible.remove(at: indice)
            return 0
        }
    }
}

extension MainViewController: CajaViewDelegate {
    
    func actualizarBd(row:Int){
        
        itemsLabel.setTitle("\(Int(itemsLabel.currentTitle!)!-stockShop[row].enCaja)",for: .normal)
        costLabel.text = "\(Float(costLabel.text!)!-stockShop[row].costoTotal())"
        if !stockShop[row].HayEnStock(){
            stockShop[row].DevolverItems()
            stockDisponible.append((stockShop[row],row))
        } else {
            stockShop[row].DevolverItems()
        }
        table.reloadData()
    }
}




